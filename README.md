# USAGE
```javascript
import { AwsClient, Subscriber, Publisher, ISQSHandler, Uploader } from "@stageit-labs/aws-client";

// Initialize aws client first
const awsClient = new AwsClient({
    region: "us-east-1",
    accessKeyId: "test",
    secretAccessKey: "test"
}, logger);

await awsClient.initialize();

// Subscriber
class MyHandler implements ISQSHandler {
    public async handle(event) {
        // do something with event
    }
}

const mySubscriber = new Subscriber(awsClient, {
    queueUrl: "<sqs-queue-url>"
});
mySubscriber.registerHandler("on-unit-created", new MyHandler());

// Publisher
const myPublisher = new Publisher(awsClient, "<sns-topic>", "source");


const event = myPublisher.createNewEvent("on-unit-updated", { unitId: 123 });
myPublisher.publish(event);

//OR use shorthand version
myPublisher.publishEvent("on-unit-updated", { unitId: 123 });


// Uploader (S3)
const uploader = new Uploader(awsClient, "my-bucket");
uploader.put("s3-key", "my-content");
```