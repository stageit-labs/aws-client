import { S3Client, ListBucketsCommand } from "@aws-sdk/client-s3";
import { SQSClient, ListQueuesCommand } from "@aws-sdk/client-sqs";
import { SNSClient, ListTopicsCommand } from "@aws-sdk/client-sns";
import { LambdaClient } from "@aws-sdk/client-lambda";
import { FirehoseClient } from "@aws-sdk/client-firehose";
import { CloudFrontClient } from "@aws-sdk/client-cloudfront";
import Subscriber from "../events/Subscriber";
import ILogger from "./ILogger";

class AwsClient {
    private _logger: ILogger;
    private _sns: SNSClient;
    private _sqs: SQSClient;
    private _s3: S3Client;
    private _cloudFront: CloudFrontClient;
    private _lambda: LambdaClient;
    private _firehose: FirehoseClient;
    private _subscribers: Subscriber[] = [];

    constructor(logger?: ILogger) {
        this._logger = logger || console;
    }

    public async initialize(): Promise<boolean> {
        try {
            this._logger.info("[AWS Module] Inititalizing AWS client...");
            this._sns = new SNSClient();
            this._sqs = new SQSClient();
            this._s3 = new S3Client();
            this._cloudFront = new CloudFrontClient();
            this._lambda = new LambdaClient();
            this._firehose = new FirehoseClient();

            await this._sns.send(new ListTopicsCommand());
            await this._sqs.send(new ListQueuesCommand());
            await this._s3.send(new ListBucketsCommand());

            this._logger.info("[AWS Module] AWS client is ready");
        } catch (e) {
            this._logger.error(e, "[AWS Module] Failed to initialize AWS client");
            throw e;
        }

        return true;
    }

    public addSubscribers(subscriber: Subscriber) {
        this._subscribers.push(subscriber);
    }

    public startAllSubscribers() {
        this._logger.info("[AWS Module] Start all subscribers");

        for (let subscriber of this._subscribers) {
            subscriber.run();
        }
    }

    public stopAllSubscribers(): void {
        this._logger.info("[AWS Module] Stop all subscribers");

        for (let subscriber of this._subscribers) {
            subscriber.stop();
        }
    }

    public SQS() {
        return this._sqs;
    }

    public SNS() {
        return this._sns;
    }

    public S3() {
        return this._s3;
    }

    public CloudFront() {
        return this._cloudFront;
    }

    public Lambda() {
        return this._lambda;
    }

    public Firehose() {
        return this._firehose;
    }

    public setLoggerInstance(logger: ILogger): void {
        this._logger = logger;
    }

    public getLoggerInstance(): ILogger {
        return this._logger;
    }
}

export default AwsClient;