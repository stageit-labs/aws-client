interface LogFn {
    /* tslint:disable:no-unnecessary-generics */
    <T extends object>(obj: T, msg?: string): void;
    (msg: string): void;
}
interface ILogger {
    error: LogFn;
    warn: LogFn;
    info: LogFn;
    debug: LogFn;
}

export default ILogger;