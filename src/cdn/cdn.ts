import AwsClient from "../base/AwsClient";
import {
    CloudFrontClient,
    CreateInvalidationCommand,
    CreateInvalidationCommandInput,
    CreateInvalidationCommandOutput,
    GetInvalidationCommand,
    GetInvalidationCommandInput,
    GetInvalidationCommandOutput,
} from "@aws-sdk/client-cloudfront";

class Cdn {
    private _awsClient: AwsClient;
    private _distributionId: string;
    constructor(awsClient: AwsClient, distributionId: string) {
        this._awsClient = awsClient;
        this._distributionId = distributionId
    }

    public getInstances(): CloudFrontClient {
        return this._awsClient.CloudFront()
    }

    public async invalidateCache(paths: string[], options: {
        distributionId?: string,
        CallerReference?: string,
    }): Promise<CreateInvalidationCommandOutput> {
        const params: CreateInvalidationCommandInput = {
            DistributionId: options.distributionId ? options.distributionId : this._distributionId,
            InvalidationBatch: {
                CallerReference: options.CallerReference ? options.CallerReference : `${Date.now()}`,
                Paths: {
                    Quantity: paths.length,
                    Items: paths
                }
            }
        };
        const command = new CreateInvalidationCommand(params);
        return this._awsClient.CloudFront().send(command);
    }

    public getInvalidation(invalidationId: string): Promise<GetInvalidationCommandOutput> {
        const params: GetInvalidationCommandInput = {
            DistributionId: this._distributionId,
            Id: invalidationId
        };
        const command = new GetInvalidationCommand(params);

        return this._awsClient.CloudFront().send(command);
    }
}

export default Cdn;