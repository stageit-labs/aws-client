import {
    _Record,
    CreateDeliveryStreamCommand,
    CreateDeliveryStreamCommandInput,
    CreateDeliveryStreamCommandOutput,
    PutRecordBatchCommand,
    PutRecordBatchCommandInput,
    PutRecordBatchCommandOutput,
} from "@aws-sdk/client-firehose";
import { AwsClient } from "..";
import ILogger from "../base/ILogger";

class Firehose {
    private _awsClient: AwsClient;
    private _logger: ILogger;

    constructor(awsClient: AwsClient) {
        this._awsClient = awsClient;
        this._logger = this._awsClient.getLoggerInstance();
    }

    public createDeliveryStream(input: CreateDeliveryStreamCommandInput): Promise<CreateDeliveryStreamCommandOutput> {
        const client = this._awsClient.Firehose();
        if (!client) {
            this._logger.error("Firehose is currently disabled");
            return;
        }

        const command = new CreateDeliveryStreamCommand(input);
        return client.send(command);
    }

    public putBatch(streamName: string, batch: any[]): Promise<PutRecordBatchCommandOutput> {
        const client = this._awsClient.Firehose();
        if (!client) {
            this._logger.error("Firehose is currently disabled");
            return;
        }

        const records: _Record[] = batch.map(data => {
            return {
                Data: data
            };
        });

        const input: PutRecordBatchCommandInput = {
            DeliveryStreamName: streamName,
            Records: records
        };

        const command = new PutRecordBatchCommand(input);
        return client.send(command);
    }
}

export default Firehose;