import { AwsClient } from "..";
import ILogger from "../base/ILogger";

interface MediaConvertParam {
    bucket: string;
    key: string;
    refId: string;
    refType: string;
}

class Lambda {
    private _awsClient: AwsClient;
    private _logger: ILogger;

    constructor(awsClient: AwsClient) {
        this._awsClient = awsClient;
        this._logger = this._awsClient.getLoggerInstance();
    }

    public invokeMediaConvert(mediaConvertParam: MediaConvertParam): Promise<any> {
        const lambda = this._awsClient.Lambda();

        if (!lambda) {
            this._logger.error("Lambda is currently disabled");
            return;
        }

        // TODO: Implement the logic to invoke the MediaConvert Lambda function
    }
}

export default Lambda;