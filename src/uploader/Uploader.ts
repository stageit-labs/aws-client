import * as stream from "stream";
import * as path from "path";
import { getSignedUrl } from "@aws-sdk/s3-request-presigner";
import {
    S3Client,
    ListBucketsCommand,
    ListBucketsCommandOutput,
    PutObjectCommand,
    PutObjectCommandInput,
    PutObjectCommandOutput,
    ObjectCannedACL,
    DeleteObjectsCommand,
    DeleteObjectsCommandInput,
    DeleteObjectsCommandOutput,
    ListObjectsV2Command,
    ListObjectsV2CommandInput,
    GetObjectCommand,
    GetObjectCommandInput,
} from "@aws-sdk/client-s3";
import { Upload } from "@aws-sdk/lib-storage"
import AwsClient from "../base/AwsClient";
import ILogger from "../base/ILogger";


class Uploader {
    private _awsClient: AwsClient;
    private _bucket: string;
    private _logger: ILogger;

    constructor(awsClient: AwsClient, bucket: string) {
        this._awsClient = awsClient;
        this._bucket = bucket;
        this._logger = awsClient.getLoggerInstance();
    }

    public getInstances(): S3Client {
        return this._awsClient.S3();
    }

    public async getBuckets(): Promise<ListBucketsCommandOutput> {
        const client = this._awsClient.S3();

        if (!client) {
            this._logger.warn("[AWS Module] S3 is not configured yet");
            return;
        }

        const command = new ListBucketsCommand();

        return client.send(command);
    }

    public async put(key: string, content: string, options: {
        contentType?: string,
        acl?: ObjectCannedACL,
        bucket?: string
    } = {}): Promise<PutObjectCommandOutput> {
        const client = this._awsClient.S3();

        if (!client) {
            this._logger.warn("[AWS Module] S3 is not configured yet");
            return;
        }

        const params: PutObjectCommandInput = {
            Bucket: options.bucket || this._bucket,
            ACL: options.acl || "public-read",
            Key: key,
            Body: content,
        };

        if (options.contentType) {
            params.ContentType = options.contentType;
        }

        const command = new PutObjectCommand(params);

        return client.send(command);
    }

    public deleteObjects(keys: string[], options: {
        bucket?: string
    }): Promise<DeleteObjectsCommandOutput> {
        const client = this._awsClient.S3();

        if (!client) {
            this._logger.warn("[AWS Module] S3 is not configured yet");
            return;
        }

        const params: DeleteObjectsCommandInput = {
            Bucket: options.bucket ? options.bucket : this._bucket,
            Delete: {
                Objects: keys.map(key => ({ Key: key }))
            }
        }

        const command = new DeleteObjectsCommand(params);

        return this._awsClient.S3().send(command);
    }

    public async clearDirectory(key: string, options: {
        bucket?: string
    } = {}): Promise<any> {
        try {
            const client = this._awsClient.S3();

            if (!this._awsClient.S3()) {
                this._logger.warn("[AWS Module] S3 is not configured yet");
                return;
            }

            const bucket = options.bucket || this._bucket;
            const listParams: ListObjectsV2CommandInput = {
                Bucket: bucket,
                Prefix: path.join(key, "/")
            };
            const listCommand = new ListObjectsV2Command(listParams);

            const commandOutput = await client.send(listCommand);

            if (!commandOutput.Contents || commandOutput.Contents.length) {
                return;
            }

            const deleteParams: DeleteObjectsCommandInput = {
                Bucket: bucket,
                Delete: {
                    Objects: []
                }
            };

            commandOutput.Contents.forEach(function (content) {
                if (content.Key) {
                    deleteParams.Delete.Objects.push({ Key: content.Key });
                }
            });

            const deleteCommand = new DeleteObjectsCommand(deleteParams);
            await client.send(deleteCommand);


            if (commandOutput.IsTruncated) {
                return this.clearDirectory(key);
            }
        } catch (error) {
            this._logger.error(error, "[AWS Module] Failed to clear directory");
            throw error;
        }
    }

    public uploadAsStream(key: string, acl?: ObjectCannedACL): [Promise<any>, stream.PassThrough] {
        const client = this._awsClient.S3();

        if (!client) {
            this._logger.warn("[AWS Module] S3 is not configured yet");
            return;
        }

        const pass = new stream.PassThrough();

        const params: PutObjectCommandInput = {
            ACL: acl || "public-read",
            Bucket: this._bucket,
            Key: key,
            Body: pass
        };

        const upload = new Upload({
            client: client,
            params: params,
        });

        const promise = upload.done();

        return [promise, pass];
    }

    public async signedUrl(url: string, expiryInSeconds: number): Promise<string> {
        const params: GetObjectCommandInput = {
            Bucket: this._bucket,
            Key: url,
        };

        const client = this._awsClient.S3();

        if (!this._awsClient.S3()) {
            this._logger.warn("[AWS Module] S3 is not configured yet");
            return;
        }

        const command = new GetObjectCommand(params);

        return getSignedUrl(client, command, { expiresIn: expiryInSeconds });
    }
}

export default Uploader;