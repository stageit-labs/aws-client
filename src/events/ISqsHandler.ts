import { CloudEvent } from "cloudevents";

interface ISqsHandler {
    /**
     * Handle event received from SQS queue.
     * @param event
     */
    handle(event: CloudEvent): any;
}

export default ISqsHandler;