import { CloudEvent } from "cloudevents";
import { PublishCommand, PublishCommandInput } from "@aws-sdk/client-sns";
import AwsClient from "../base/AwsClient";
import ILogger from "../base/ILogger";

const DEFAULT_PRIORITY = 5;

interface PublishOptions extends PublishCommandInput {
    priority?: number;
}

class Publisher {
    protected source: string;
    private _awsClient: AwsClient;
    private _topic: string;
    private _logger: ILogger;

    constructor(awsClient: AwsClient, topic: string, source: string) {
        if (!topic) {
            throw new Error(`[AWS Module] Invalid SNS topic '${topic}'`);
        }

        this._awsClient = awsClient;
        this._topic = topic;
        this.source = source;
        this._logger = this._awsClient.getLoggerInstance();
    }

    public async publish(
        event: Partial<CloudEvent> | CloudEvent,
        options?: Partial<PublishOptions>
    ) {
        const sns = this._awsClient.SNS();

        if (!sns) {
            this._logger.warn("[AWS Module] SNS is not configured yet");
            return;
        }

        const cloudevent = this.toCloudevent(event);
        const eventMessage = cloudevent.toString();
        options = this.mergeDefaultAttribute(cloudevent, options);

        const input = {
            TopicArn: this._topic,
            Message: eventMessage,
            ...options,
        } as PublishCommandInput;
        const command = new PublishCommand(input);

        try {
            await sns.send(command);
            this._logger.info({
                topic: this._topic,
                data: eventMessage,
            }, "[AWS Module] Publish mesasge successfully");
        } catch (error) {
            this._logger.error({
                topic: this._topic,
                data: event,
                error,
            }, "[AWS Module] Failed to publish message");
            throw error;
        }
    }

    public publishEvent(type: string, data: any, options?: Partial<PublishOptions>) {
        const event = this.createNewEvent(type, data);
        return this.publish(event, options);
    }

    public createNewEvent(type: string, data: any): CloudEvent {
        return { source: this.source, type, data } as CloudEvent;
    }

    private mergeDefaultAttribute(cloudevent: CloudEvent, options: Partial<PublishOptions> = {}) {
        return {
            ...options,
            MessageAttributes: {
                ...options.MessageAttributes,
                source: {
                    StringValue: cloudevent.source,
                    DataType: "String"
                },
                eventName: {
                    StringValue: cloudevent.type,
                    DataType: "String"
                },
                environment: {
                    StringValue: process.env.ENVIRONMENT || "default",
                    DataType: "String"
                },
                priority: {
                    StringValue: options.priority?.toString() || DEFAULT_PRIORITY.toString(),
                    DataType: "Number"
                }
            }
        }
    }

    private toCloudevent(event: Partial<CloudEvent>): CloudEvent {
        if (event instanceof CloudEvent) {
            return event.cloneWith({ source: this.source });
        }

        event.source = this.source;
        return new CloudEvent(event as CloudEvent);
    }
}

export default Publisher;