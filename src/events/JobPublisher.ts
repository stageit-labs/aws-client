import { CloudEvent } from "cloudevents";
import Publisher from "./Publisher";

const DELAYED_QUEUE_NAME = "general";
const JOB_EVENT_TYPE = {
    ADDED: "com.icondo.job.added",
    UPDATED: "com.icondo.job.updated",
    DELETED: "com.icondo.job.deleted",
    DELAY: "com.icondo.job.delay",
};

interface JobOptions {
    executeTime?: any;
    delay?: number;
    jobId?: string;
    overrideExisting?: boolean;
}

interface UpsertJobDataType {
    queueName: string;
    jobType: string;
    jobData: any;
    jobOptions?: JobOptions;
}

interface DeleteJobDataType {
    queueName: string;
    jobType: string;
    jobOptions: {
        jobId: string
    };
}

class JobPublisher extends Publisher {
    public publishJobCreatedEvent(data: UpsertJobDataType): Promise<any> {
        const event = this.createJobAddedEvent(data);
        return this.publish(event);
    }

    public publishJobUpdatedEvent(data: UpsertJobDataType): Promise<any> {
        const event = this.createJobUpdatedEvent(data);
        return this.publish(event);
    }

    public publishJobDeletedEvent(data: DeleteJobDataType): Promise<any> {
        const event = this.createJobDeletedEvent(data);
        return this.publish(event);
    }

    public publishDelyedJobCreatedEvent(callbackType: string, callbackData: any, options: {
        executeTime?: string,
        jobId?: string,
    }): Promise<any> {
        const event = this.createJobAddedEvent({
            jobType: JOB_EVENT_TYPE.DELAY,
            queueName: DELAYED_QUEUE_NAME,
            jobData: { callbackType, callbackData },
            jobOptions: {
                executeTime: options.executeTime,
                jobId: options.jobId
            }
        });
        return this.publish(event);
    }

    public publishDelyedJobUpdatedEvent(callbackType: string, callbackData: any, options: {
        executeTime?: string,
        jobId: string,
    }): Promise<any> {
        const event = this.createJobUpdatedEvent({
            jobType: JOB_EVENT_TYPE.DELAY,
            queueName: DELAYED_QUEUE_NAME,
            jobData: { callbackType, callbackData },
            jobOptions: {
                executeTime: options.executeTime,
                jobId: options.jobId
            }
        });
        return this.publish(event);
    }

    public publishDelyedJobDeletedEvent(jobId: string): Promise<any> {
        const event = this.createJobDeletedEvent({
            jobType: JOB_EVENT_TYPE.DELAY,
            queueName: DELAYED_QUEUE_NAME,
            jobOptions: {
                jobId
            }
        })
        return this.publish(event);
    }

    public createJobAddedEvent(data: UpsertJobDataType): CloudEvent {
        return {
            source: this.source,
            type: JOB_EVENT_TYPE.ADDED,
            data,
        } as CloudEvent;
    }

    public createJobUpdatedEvent(data: UpsertJobDataType): CloudEvent {
        return {
            source: this.source,
            type: JOB_EVENT_TYPE.UPDATED,
            data,
        } as CloudEvent;
    }

    public createJobDeletedEvent(data: DeleteJobDataType): CloudEvent {
        return {
            source: this.source,
            type: JOB_EVENT_TYPE.DELETED,
            data,
        } as CloudEvent;
    }
}

export default JobPublisher;