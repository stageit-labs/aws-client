import { Consumer, ConsumerOptions } from "sqs-consumer";
import { Message } from "@aws-sdk/client-sqs";
import { CloudEvent } from "cloudevents";
import AwsClient from "../base/AwsClient";
import ISqsHandler from "./ISqsHandler";
import ILogger from "../base/ILogger";

export class Subscriber {
    private _awsClient: AwsClient;
    private _client: Consumer;
    private _handlers: { type: string, handler: ISqsHandler }[] = [];
    private _logger: ILogger;
    private _opts: ConsumerOptions;

    constructor(awsClient: AwsClient, opts: ConsumerOptions) {
        this._awsClient = awsClient;
        this._logger = this._awsClient.getLoggerInstance();

        if (!opts.queueUrl) {
            this._logger.error("[AWS Module] Cannot load queueUrl from configuration");
            throw new Error("[AWS Module] Cannot load queueUrl from configuration");
        }

        this._opts = opts;
    }

    /**
     * Handle message received from queue
     * @param messages
     */
    public async handleMessage(message: Message): Promise<any> {
        try {
            const body = JSON.parse(message.Body);
            const event = new CloudEvent(JSON.parse(body.Message));
            const handlers = this._handlers.filter(handler => handler.type === event.type);
            if (handlers.length) {
                for (let { handler } of handlers) {
                    await handler.handle(event);
                }
            }
        } catch (err) {
            this._logger.error(err, err.message);
        }
    }

    /**
     * Register a handler for a specific clound event type
     * @param cloudEventType
     * @param handler
     */
    public registerHandler(cloudEventType: string, handler: ISqsHandler): void {
        this._handlers.push({ type: cloudEventType, handler });
    }

    /**
     * Clear all handlers
     */
    public clearHandlers(): void {
        this._handlers = [];
    }

    public init() {
        try {
            this._client = Consumer.create({
                terminateVisibilityTimeout: true,
                visibilityTimeout: 60,
                waitTimeSeconds: 20,
                batchSize: 1,
                ...this._opts,
                handleMessage: this.handleMessage.bind(this),
                sqs: this._awsClient.SQS(),
            });

            this._awsClient.addSubscribers(this);

            this._client.on("error", (err) => {
                this._logger.error(err, "[SQSConsumer] Error occurred");
            });

            this._client.on("stopped", () => {
                this._logger.debug("[SQSConsumer] Client Stopped!!!");
            });

            this._client.on("processing_error", (err) => {
                this._logger.error(err, "[SQSConsumer] Processing error occurred");
            });

            this._logger.info("[AWS Module] Subscriber is ready");
        } catch (err) {
            this._logger.error(err, err.message);
            this._logger.warn("[AWS Module] Subscriber will be disabled");
        }
    }

    public run() {
        try {
            if (this._client && !this._client.status.isRunning) {
                this._client.start();
            }
        } catch (err) {
            this._logger.error(err, err.message);
        }
    }

    public stop() {
        if (this._client && this._client.status.isRunning) {
            this._client.stop();
        }
    }
}

export default Subscriber;
